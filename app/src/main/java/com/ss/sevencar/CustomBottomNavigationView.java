package com.ss.sevencar;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

public class CustomBottomNavigationView extends BottomNavigationView {
    public CustomBottomNavigationView(Context context) {
        super(context);
    }

    public CustomBottomNavigationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomBottomNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public LiveData<NavController> setup(List<Integer> navGraphIds, final FragmentManager fragmentManager, int containerId, Intent intent) {
        final SparseArray<String> graphIdToTagMap = new SparseArray<>();
        // Result. Mutable live data with the selected controlled
        MutableLiveData<NavController> selectedNavController = new MutableLiveData<NavController>();

        int firstFragmentGraphId = 0;

        // First create a NavHostFragment for each NavGraph ID

        for (int i = 0; i < navGraphIds.size(); i++) {
            String fragmentTag = getFragmentTag(i);

            // Find or create the Navigation host fragment
            NavHostFragment navHostFragment = obtainNavHostFragment(
                    fragmentManager,
                    fragmentTag,
                    navGraphIds.get(i),
                    containerId
            );

            // Obtain its id
            int graphId = navHostFragment.getNavController().getGraph().getId();

            if (i == 0) {
                firstFragmentGraphId = graphId;
            }

            // Save to the map
            graphIdToTagMap.setValueAt(graphId, fragmentTag);

            // Attach or detach nav host fragment depending on whether it's the selected item.
            if (this.getSelectedItemId() == graphId) {
                // Update livedata with the selected graph
                selectedNavController.setValue(navHostFragment.getNavController());
                attachNavHostFragment(fragmentManager, navHostFragment, i == 0);
            } else {
                detachNavHostFragment(fragmentManager, navHostFragment);
            }
        }

        // Now connect selecting an item with swapping Fragments
        int selectedItemId = this.getSelectedItemId();
        final String selectedItemTag = graphIdToTagMap.get(selectedItemId);
        final String firstFragmentTag = graphIdToTagMap.get(firstFragmentGraphId);
        boolean isOnFirstFragment = selectedItemTag.equalsIgnoreCase(firstFragmentTag);
        setOnNavigationItemSelectedListener(new OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (fragmentManager.isStateSaved()) {
                    return false;
                } else {
                    String newlySelectedItemTag = graphIdToTagMap.get(item.getItemId());
                    if (selectedItemTag.equalsIgnoreCase(newlySelectedItemTag)) {
                        // Pop everything above the first fragment (the "fixed start destination")
                        fragmentManager.popBackStack(firstFragmentTag,
                                FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        NavHostFragment selectedFragment = (NavHostFragment) fragmentManager.findFragmentByTag(newlySelectedItemTag);

                        // Exclude the first fragment tag because it's always in the back stack.
                        if (firstFragmentTag.equalsIgnoreCase(newlySelectedItemTag)) {
                            // Commit a transaction that cleans the back stack and adds the first fragment
                            // to it, creating the fixed started destination.
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction()
                                    .attach(selectedFragment)
                                    .setPrimaryNavigationFragment(selectedFragment);

                            for (int i = 0; i < graphIdToTagMap.size(); i++) {
                                if (graphIdToTagMap.get(i).equalsIgnoreCase()) {
                                    detach(fragmentManager.findFragmentByTag(firstFragmentTag);
                                    !!)
                                }
                            }

                            fragmentTransaction.addToBackStack(firstFragmentTag)
                                    .setCustomAnimations(
                                            R.anim.nav_default_enter_anim,
                                            R.anim.nav_default_exit_anim,
                                            R.anim.nav_default_pop_enter_anim,
                                            R.anim.nav_default_pop_exit_anim)
                                    .setReorderingAllowed(true)
                                    .commit();
                        }
                        selectedItemTag = newlySelectedItemTag;
                        isOnFirstFragment = selectedItemTag == firstFragmentTag
                        selectedNavController.value = selectedFragment.navController
                        true
                    } else {
                        false
                    }
                }
            });


            // Finally, ensure that we update our BottomNavigationView when the back stack changes
            fragmentManager.addOnBackStackChangedListener

            {
                if (!isOnFirstFragment && !fragmentManager.isOnBackStack(firstFragmentTag)) {
                    this.selectedItemId = firstFragmentGraphId
                }

                // Reset the graph if the currentDestination is not valid (happens when the back
                // stack is popped after using the back button).
                selectedNavController.value ?.let {
                controller ->
                if (controller.currentDestination == null) {
                    controller.navigate(controller.graph.id)
                }
            }
            }
        return selectedNavController;
        }

        private String getFragmentTag ( int index){
            return "bottomNavigation#" + index;
        }

        private NavHostFragment obtainNavHostFragment (
                FragmentManager fragmentManager,
                String fragmentTag,
        int navGraphId,
        int containerId
    ){
            // If the Nav Host fragment exists, return it
            NavHostFragment existingFragment = (NavHostFragment) fragmentManager.findFragmentByTag(fragmentTag);
            if (existingFragment != null)
                return existingFragment;

            // Otherwise, create it and return it.
            NavHostFragment navHostFragment = NavHostFragment.create(navGraphId);
            fragmentManager.beginTransaction()
                    .add(containerId, navHostFragment, fragmentTag)
                    .commitNow();
            return navHostFragment;
        }

        private void attachNavHostFragment (
                FragmentManager fragmentManager,
                NavHostFragment navHostFragment,
        boolean isPrimaryNavFragment
    ){
            FragmentTransaction fragmentTransactions = fragmentManager.beginTransaction()
                    .attach(navHostFragment);

            if (isPrimaryNavFragment) {
                fragmentTransactions.setPrimaryNavigationFragment(navHostFragment);
            }

            fragmentTransactions.commitNow();

        }

        private void detachNavHostFragment (
                FragmentManager fragmentManager,
                NavHostFragment navHostFragment
    ){
            fragmentManager.beginTransaction()
                    .detach(navHostFragment)
                    .commitNow();
        }

    }
